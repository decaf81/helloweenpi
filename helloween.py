import RPi.GPIO as GPIO ## Import GPIO library
import time ## Import 'time' library. Allows us to use 'sleep'
import threading
import random
from threading import Thread
import socket

demo = 0
fire = 32
GPIO.setmode(GPIO.BOARD) ## Use board pin numbering
GPIO.setup(7, GPIO.OUT) ## Setup GPIO Pin 7 to OUT
GPIO.setup(11, GPIO.IN)
GPIO.setup(40, GPIO.OUT)
GPIO.setup(38, GPIO.OUT)
GPIO.setup(32, GPIO.OUT)


class ThreadedServer(object):
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))

    def listen(self):
        self.sock.listen(5)
        while True:
            client, address = self.sock.accept()
            client.settimeout(60)
            threading.Thread(target = self.listenToClient,args = (client,address)).start()

    def listenToClient(self, client, address):
        size = 1024
        while True:
            try:
                data = client.recv(size)
                if data:
                     try:
                        volume = int(data)
 
                     except:
                        print 'NACK:', data
                else:
                    raise error('Client disconnected')
            except:
                client.close()
                return False
        
def sendCommand():
	x = 0
	global audio
	while True:
	   time.sleep(0.5)
	   print "SendCommand is sleeping",x,audio
	   if x != audio:
		print "sendCommmand detected a change" 
		x = audio
		if x == 1:
			x = audio
        		TCP_IP = '192.168.178.194'
        		TCP_PORT = 5010
        		BUFFER_SIZE = 1024
        		MESSAGE = "Play"
			
        		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        		s.connect((TCP_IP, TCP_PORT))
        		s.send(MESSAGE)
        		#data = s.ddrecv(BUFFER_SIZE)
        		s.close()
	 	if x == 0:
			x = audio
                	TCP_IP = '192.168.178.194'
                	TCP_PORT = 5010
                	BUFFER_SIZE = 1024
                	MESSAGE = "Stop"

                	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                	s.connect((TCP_IP, TCP_PORT))
                	s.send(MESSAGE)
                	#data = s.recv(BUFFER_SIZE)
                	s.close()

def fire():
	GPIO.output(32,0)
	time.sleep(1)
	GPIO.output(32,1)


def ghostblink():
	global audio
	audio = 1
	time.sleep(1)
	for i in range(0,90):
		x = random.randrange(20)
		z = random.uniform(0,0.20)
		
		print z

		if (i == 45):
			t = Thread(target=fire)
			t.start()

		if (x % 2 == 0):
			GPIO.output(40, not GPIO.input(40))
			time.sleep(z)
		else:
			GPIO.output(38, not GPIO.input(38))
			time.sleep(z)

	GPIO.output(38,1)
	GPIO.output(40,1)
	print "Idling"
	audio = 0
	time.sleep(10)
	print "Exiting def"

def fastblink():
	relay = 0
	GPIO.output(40, True)
	for i in range (0,20):
		GPIO.output(40, not GPIO.input(40))
		time.sleep(.009)
		GPIO.output(38, not GPIO.input(38))
		time.sleep(.009)

def handler():
       y = 1
       for i in range(0,10):## Run loop numTimes
               #print "Iteration " + str(i+1)## Print current loop
               print y
               GPIO.output(7,True)## Switch on pin 7
               time.sleep(1)## Wait
               GPIO.output(7,False)## Switch off pin 7
               time.sleep(1)## Wait
       print "Done" ## When loop is complete, print "Done"
       y = 0
       return

y = 0
audio = 0
GPIO.output(32,1)
#t = Thread(target=fire)
print "Going to loop"
port_num = 5011
GPIO.output(40, not GPIO.input(40))
GPIO.output(38, not GPIO.input(38))
#ThreadedServer('',port_num).listen()
t = Thread(target=sendCommand, args=())
t.start()
if demo == 0:
 print "Non demo loop"
 while True:
       i=GPIO.input(11)
       time.sleep(0.1)
       if i==0:                 #When output from motion sensor is LOW
             #print "No intruders",i
             pass
       elif i==1:               #When output from motion sensor is HIGH
             print "Intruder detected",i
             ghostblink()
else:
 print "Demo loop"
 t = 0
 while True:
  for t in range (0, 60):
   t = t + 1
   print t
   time.sleep(1)
   if t == 10:
    ghostblink()

