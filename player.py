import vlc
import socket
import threading
import time
class ThreadedServer(object):
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))

    def listen(self):
        self.sock.listen(5)
        while True:
            client, address = self.sock.accept()
            client.settimeout(60)
            threading.Thread(target = self.listenToClient,args = (client,address)).start()

    def listenToClient(self, client, address):
        size = 1024
	#p = vlc.MediaPlayer('file:///home/cblad/Ghost01.mp3')
        while True:
            try:
                data = client.recv(size)
                if data:
                    # Set the response to echo back the recieved data 
                    response = data
		    commando = str(response)
		    print commando,"EOF"
		    #ycommando = commando[:-2]
		    if commando == 'Play':
		    	try:
				#p = vlc.MediaPlayer('file:///home/cblad/Ghost01.mp3')
		        	#time.sleep (0.5) 
		        	player.play()
		    	except:
				print "START failed"
 
		    if commando == 'Stop':
			print "Stopping" 
			try:
				player.stop()
			except:
				print " STOP failed" 
      
                else:
                    raise error('Client disconnected')
            except:
                client.close()
                return False

if __name__ == "__main__":
    port_num = 5010
    instance = vlc.Instance()
    player = instance.media_player_new()
    player.set_media(instance.media_new('file:///home/cblad/Ghost01.mp3'))
    ThreadedServer('',port_num).listen()
