import RPi.GPIO as GPIO ## Import GPIO library
import time ## Import 'time' library. Allows us to use 'sleep'
import threading
import random
from threading import Thread

fire = 32
GPIO.setmode(GPIO.BOARD) ## Use board pin numbering
GPIO.setup(7, GPIO.OUT) ## Setup GPIO Pin 7 to OUT
GPIO.setup(11, GPIO.IN)
GPIO.setup(40, GPIO.OUT)
GPIO.setup(38, GPIO.OUT)
GPIO.setup(32, GPIO.OUT)

def fire():
	GPIO.output(32,0)
	time.sleep(1)
	GPIO.output(32,1)


def ghostblink():
	time.sleep(1)
	for i in range(0,90):
		x = random.randrange(20)
		z = random.uniform(0,0.20)
		
		print z

		if (i == 45):
			t = Thread(target=fire)
			t.start()

		if (x % 2 == 0):
			GPIO.output(40, not GPIO.input(40))
			time.sleep(z)
		else:
			GPIO.output(38, not GPIO.input(38))
			time.sleep(z)

	GPIO.output(38,0)
	GPIO.output(40,0)
	print "Idling"
	time.sleep(10)
	print "Exiting def"

def fastblink():
	relay = 0
	GPIO.output(40, True)
	for i in range (0,20):
		GPIO.output(40, not GPIO.input(40))
		time.sleep(.009)
		GPIO.output(38, not GPIO.input(38))
		time.sleep(.009)

def handler():
       y = 1
       for i in range(0,10):## Run loop numTimes
               #print "Iteration " + str(i+1)## Print current loop
               print y
               GPIO.output(7,True)## Switch on pin 7
               time.sleep(1)## Wait
               GPIO.output(7,False)## Switch off pin 7
               time.sleep(1)## Wait
       print "Done" ## When loop is complete, print "Done"
       y = 0
       return

y = 0
GPIO.output(32,1)
#t = Thread(target=fire)
print "Going to loop"

while True:
       i=GPIO.input(11)
       if i==0:                 #When output from motion sensor is LOW
             #print "No intruders",i
	     pass
       elif i==1:               #When output from motion sensor is HIGH
             print "Intruder detected",i
             ghostblink()
